new Vue({
	el : '#products-container',

	data: {

		infos:false,
		name:null,
		size: null,
		sequence:null
	},

	methods: {

		showInfos: function(size, name, sequence)
		{
			this.infos = true;
			this.name = name;
			this.size = size;
			this.sequence = sequence;

			$('.carousel').carousel().carousel('pause');

		},

		closeInfos: function()
		{
			this.infos = false;
		}
	}

});