<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('master');
});

Route::get('/contact',function(){
	return view('contact');
});


Route::get('/about',function(){
	return view('about');
});


Route::get('/beirut',function(){

	$products = [
		[
			'name'=> 'HAMRA (HSBC)',
			'size' => '4 x 3 Meters',
			'sequence' => '10 seconds',
			'src' => '/data/hamra-hsbc.png'
		],
		[
			'name'=> 'KARAKAS (1)',
			'size' => '4 x 3 Meters',
			'sequence' => '10 seconds',
			'src' => '/data/KARAKAS-1.png'
		],
		[
			'name'=> 'KARAKAS (2)',
			'size' => '4 x 3 Meters',
			'sequence' => '10 seconds',
			'src' => '/data/KARAKAS-2.png'
		],
		[
			'name'=> 'RAMLET EL BAYDA (1)',
			'size' => '4 x 3 Meters',
			'sequence' => '10 seconds',
			'src' => '/data/RAMLET-BAYDA-1.png'
		],
		[
			'name'=> 'RAMLET EL BAYDA (2)',
			'size' => '4 x 3 Meters',
			'sequence' => '10 seconds',
			'src' => '/data/RAMLET-BAYDA-2.png'
		],
		[
			'name'=> 'SAKIET AL JANZIR (1)',
			'size' => '4 x 3 Meters',
			'sequence' => '10 seconds',
			'src' => '/data/SAKIET-1.png'
		],
		[
			'name'=> 'SAKIET AL JANZIR (2)',
			'size' => '4 x 3 Meters',
			'sequence' => '10 seconds',
			'src' => '/data/SAKIET-2.png'
		],
		[
			'name'=> 'VERDUN ()',
			'size' => '4 x 3 Meters',
			'sequence' => '10 seconds',
			'src' => '/data/VERDUN-1.png'
		],
		[
			'name'=> 'VERDUN (2)',
			'size' => '4 x 3 Meters',
			'sequence' => '10 seconds',
			'src' => '/data/VERDUN-2.png'
		]
	];

	$title = "Beirut Backlits";


	return view('products',compact('products','title'));
});

Route::get('/led', function(){

		
	$products = [
		[
			'name'=> 'HAMRA (Fransabank)',
			'size' => '4 x 3 Meters',
			'sequence' => '10 seconds',
			'src' => '/data/led-hamra.png'
		],
		[
			'name'=> 'HAMRA (Wardieh)',
			'size' => '4 x 3 Meters',
			'sequence' => '10 seconds',
			'src'=>'/data/led-wardieh.png'
		],
		[
			'name'=> 'HAMRA (Bristol)',
			'size' => '4 x 3 Meters',
			'sequence' => '10 seconds',
			'src'=>'/data/led-bristol.png'
		],
		[
			'name'=> 'HAMRA (street end)',
			'size' => '3 x 3 Meters',
			'sequence' => '10 seconds',
			'src'=>'/data/led-street-end.png'
		],
		[
			'name'=> 'BLISS (Orient)',
			'size' => '6 x 3 Meters',
			'sequence' => '10 seconds',
			'src'=>'/data/led-orient.png'
		],
		[
			'name'=> 'ACHRAFIEH (IBL)',
			'size' => '3 x 3 Meters',
			'sequence' => '10 seconds',
			'src'=>'/data/led-ibl.png'
		],
		[
			'name'=> 'MANARA (Riadi)',
			'size' => '4 x 3 Meters',
			'sequence' => '10 seconds',
			'src'=>'/data/led-riyadi.png'
		],
		[
			'name'=> 'SAIFI',
			'size' => '4 x 3 Meters',
			'sequence' => '10 seconds',
			'src'=>'/data/led-saifi.png'
		]
	];

	$title = "Our LED Network";

	return view('products',compact('products','title'));

});


Route::get('/map',function(){
	return view('map');
});