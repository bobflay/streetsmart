@extends('welcome')
@section('content')
<div class="row">
	<p class="section-title">
		{{$title}}
	</p>
</div>

<div class="row" id="products-container">

	<div id="products-carousel"  class="carousel slide" data-ride="carousel">
	  <!-- Indicators -->
	  <ol class="carousel-indicators">
	  	@foreach($products as $key=>$product)
	  		<li data-target="#products-carousel" data-slide-to="{{$key}}" class="active"></li>
	  	@endforeach
	  </ol>

	  <!-- Wrapper for slides -->
	  <div class="carousel-inner"  role="listbox">
	  		 <div class="item active">
	  		 	<div class="data-container">
	  		 		<img src="{{$products[0]['src']}}" class="img-responsive" alt="{{$products[0]['name']}}">
	  		 		<img src="/info-btn.png" class="img-responsive info-btn pull-right">
	  		 	</div>
	    	</div>
	  	@foreach($products as $product)
	    	<div class="item">
	      		<img src="{{$product['src']}}" alt="{{$product['name']}}">
	      		<img src="/info-btn.png" v-on:click="showInfos('{{$product['size']}}','{{$product['name']}}','{{$product['sequence']}}')" class="img-responsive info-btn pull-right">
	    	</div>
	    @endforeach
	  </div>

	  <!-- Controls -->
	  <a class="left carousel-control" id="left-control" href="#products-carousel" role="button" data-slide="prev">
	    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
	    <span class="sr-only">Previous</span>
	  </a>
	  <a class="right carousel-control" id="right-control" href="#products-carousel" role="button" data-slide="next">
	    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
	    <span class="sr-only">Next</span>
	  </a>
	  	<div class="data-box"  v-show='infos'>
	  		<img src="/close-btn.png" v-on:click="closeInfos()" id="close-btn">
	  		<h3 id="info-name">@{{name}}</h3>
	  		<img id="seperator" src="/seperator.png">
	  		<div>
	  			<p id="size-title"><strong>Size</strong></p>
	  			<p id="size-container">@{{size}}</p>

	  			<p v-show="sequence" id="sequence-title"><strong>Sequence</strong></p>
	  			<p v-show="sequence" id="sequence-container">@{{sequence}}</p>
	  		</div>
		</div>
	</div>
	
</div>

<script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.7/vue.js"></script>
<script src="/app.js"></script>
@endsection