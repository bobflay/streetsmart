<div class="home-container">
	<div id="welcome-row" class="row">
		<img id="welcome-img" src="/welcome.png">
	</div
	<div class="row">
		<p class="text-center" id="eagle-eye">
			The Eagle Eye
		</p>
	</div>
	<div class="row">
		<div class="about-txt">
			<p class="text-center">
				Perception is a fundamental quality in the world of advertising, but it means much more than just “to be seen”...
			</p>
			<p class="text-center">
				Introducing a new angle to brand exposure and communication is Street Smart’s way of distinguishing the elements and parameters that allow an ad to perform at its full potential.
			</p>

			<p class="text-center">
				Through the Eagle Eye philosophy, Street Smart holds the superior ability to handpick the elite locations, offering a new form of communication: targeted media.
			</p>
		</div>
	</div>

	<div class="row social-row">
		<div class="col-xs-6 col-sm-6 col-md-6">
			<a href="#">
				<img src="/facebook.png" class="pull-right social-icon">
			</a>
		</div>
		<div class="col-xs-6 col-sm-6 col-md-6">
			<a href="#">
				<img src="/instagram.png" class="social-icon">
			</a>
		</div>
	</div>
</div>
