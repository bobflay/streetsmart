@extends('welcome')
@section('content')
	<div class="row">
		<p class="section-title">
			Contact
		</p>
	</div>

	<div class="row" id="contact-container">
		<div class="col-xs-6 col-sm-6 col-md-6" id="contact-image-container">
			<img src="/birds.png" id="image-contact">
		</div>
		<div class="col-xs-6 col-sm-6 col-md-6">
			<div class="row contact-row">
				<div class="col-xs-1 col-sm-1 col-md-1 contact-lable">
					<div id="email-lable" class="vertical-text">
						<strong>
							EMAIL
						</strong>
					</div>
				</div>
				<div class="col-xs-1 col-sm-1 col-md-1 vertical-seperator">
					<div  class="vertical-ligne">
					</div>
				</div>
				<div class="col-xs-10 col-sm-10 col-md-10">
					<p>Drop us a line:</p>
					<p><strong>sales@street-smart.me</strong></p>
				</div>
			</div>

			<div class="row phone-row">
				<div class="col-xs-1 col-sm-1 col-md-1 contact-lable">
					<div id="email-lable" class="vertical-text">
						<strong>
							PHONE
						</strong>
					</div>
				</div>
				<div class="col-xs-1 col-sm-1 col-md-1 vertical-seperator">
					<div  class="vertical-ligne">
					</div>
				</div>
				<div class="col-xs-10 col-sm-10 col-md-10">
					<p>Give us a call:</p>
					<p><strong>(961) 70 111136</strong></p>
				</div>
			</div>
			<form id="message-form">
				<div class="row">
					<h3>Or leave us a message</h3>
					<div class="form-group">
						<label for="name">Name</label>
						<input type='text' id="name" class="form-control" name="name">
					</div>
					<div class="form-group">
						<label for="name">Email</label>
						<input type='text' id="email" class="form-control" name="email">
					</div>
					<div class="form-group">
						<label for="name">Message</label>
						<textarea id="message" class="form-control" name="message"></textarea>
					</div>

					<img src="/submit.png" id="submit-btn">
				</div>
			</form>


		</div>
	</div>
@endsection