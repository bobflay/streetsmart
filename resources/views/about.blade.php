@extends('welcome')
@section('content')
<div class="row">
	<p class="text-center about-title">
		THE COMPANY
	</p>
</div>

<div class="row" id="about-container">
	<div class="col-xs-6 col-sm-6 col-md-6" id="about-image-container">
		<img src="the-company.png" class="img-responsive">
	</div>
	<div class="col-xs-6 col-sm-6 col-md-6" id="about-right">
		<div class="row paragraph-title">
			<div class="col-xs-8 col-sm-8 col-md-8 paragraph-header-container">
				<h3 class='paragraph-header'>Founded in 2010</h3>
			</div>

			<div class="col-xs-4 col-sm-4 col-md-4 paragraph-subfix">
				<p>(THE REVOLUTION)</p>
			</div>
		</div>

		<div class="row paragraph-body">
			Street Smart began at the time as the 2nd company in Lebanon to introduce the pioneering LED technology. It launched with its first interactive billboard, strategically placed on the façade of the Orient Queen Homes, next to the American University of Beirut, that remains the only prominent digital display in this busy spot to this day.
			<br>
			<br>
			The Street Smart venture evolved through the years to have it become one of Beirut’s leading digital networks, offering full-fledged coverage in the busiest areas of the city and aiming to always offer more than meets the eye.
		</div>


		<div class="row paragraph-title">
			<div class="col-xs-8 col-sm-8 col-md-8 paragraph-header-container">
				<h3 class='paragraph-header'>Strategically placed</h3>
			</div>

			<div class="col-xs-4 col-sm-4 col-md-4 paragraph-subfix">
				<p>(SINGULARITY)</p>
			</div>
		</div>

		<div class="row paragraph-body">
			Street Smart focuses on the display’s surrounding area to handpick the spot that will
			best fit its clientele – mainly at the heart of business centers and next to universities.
			<br>
			<br>
			The Hamra network, for example, has the benefit of being exclusive to Street Smart. With no other coverage in the area, its dedicated LED billboards are strategically placed at all main entrances and exits.
		</div>


		<div class="row paragraph-title">
			<div class="col-xs-8 col-sm-8 col-md-8 paragraph-header-container">
				<h3 class='paragraph-header'>Expand&enhance</h3>
			</div>

			<div class="col-xs-4 col-sm-4 col-md-4 paragraph-subfix">
				<p>(INNOVATION)</p>
			</div>
		</div>

		<div class="row paragraph-body">
			Street Smart focuses on the display’s surrounding area to handpick the spot that will
			best fit its clientele – mainly at the heart of business centers and next to universities.
			<br>
			<br>
			The Hamra network, for example, has the benefit of being exclusive to Street Smart. With no other coverage in the area, its dedicated LED billboards are strategically placed at all main entrances and exits.
		</div>



	</div>

</div>
@endsection