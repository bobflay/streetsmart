@extends('welcome')
@section('content')
	<div id="map">
	</div>


	<script type="text/javascript">

	function initMap() {

		 var map = new google.maps.Map(document.getElementById('map'), {
		    zoom: 15,
		    center: {lat: 33.8931032, lng: 35.4913545},
		  	disableDefaultUI: true

		  });

		var image = '/marker.png';

		var locations = [
			[33.895232, 35.487505],
			[33.896279, 35.488013],
			[33.892024, 35.483706],
			[33.896335, 35.478219],
			[33.899636, 35.486927],
			[33.892003, 35.515692],
			[33.896923, 35.470867],
			[33.889638, 35.506790],
			[33.896103, 35.483778],
			[33.898247, 35.475229],
			[33.894097, 35.477236],
			[33.878833, 35.483174],
			[33.877643, 35.481320],
			[33.888805, 35.479785],
			[33.889690, 35.477275],
			[33.889158, 35.483393],
			[33.888895, 35.482400]

		];



		for (var i = locations.length - 1; i >= 0; i--) {
			var beachMarker = new google.maps.Marker({
			    position: {lat: locations[i][0], lng: locations[i][1] },
			    map: map,
			    icon: image
		 	});
		};

		
	}

	</script>

	<script async defer
        src="https://maps.googleapis.com/maps/api/js?callback=initMap"></script>
@endsection
