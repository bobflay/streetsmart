<div class="row footer-container">
	<div class="col-md-10">
		<p class="rights">
			©2015 All Rights Reserved. Designed & Developed by Stroberry Advertising
		</p>
	</div>
	<div class="col-md-2">
		<p class="mail">
			info@street-smart.me
		</p>
	</div>
</div>