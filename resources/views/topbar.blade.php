<div class="row topbar">
	<div class="col-xs-6 col-sm-6 col-md-6">
		<a href="/">
			<img class="logo" src="/logo.png">
		</a>
	</div>
	<div class="col-xs-6 col-sm-6 col-md-6">
		<nav>
			<ul class="nav-list">
				<li class="nav-item"><a href="/about">The Company</a></li>
        		<li class="nav-item dropdown">
          			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Products <span class="caret"></span></a>
          				<ul class="dropdown-menu">
	            			<li><a href="/beirut">Beirut Backlits</a></li>
	            			<li><a href="/led">LED</a></li>
	            			<li><a href="/hamra">Hamra Network</a></li>
	 						<li><a href="/creative-media">Creative Media</a>
         				</ul>
        		</li>				
        		<li class="nav-item"><a href="/map">Strategic Locations</a></li>
        		<li class="nav-item"><a href="/contact">Contact</a></li>
			</ul>
		</nav>
	</div>

	<img class="shadowbar" src="/shadowbar.png">
</div>


